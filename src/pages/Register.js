
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2'

export default function Register(){

	const { user } = useContext(UserContext);

	// State hooks to store the values of input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(password);
	console.log(mobileNumber);



	function registerUser(event){
		// Prevents page redirection via form submission
		event.preventDefault();
		
		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: { 'Content-Type' : 'application/json'},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				mobileNumber: mobileNumber
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Registration Successful",
					icon: "success",
					text: "Welcome to Zuitt"
				})

			}
			else{
				Swal.fire({
					title: "Registration Failed",
					icon: "error",
					text: "Please try again"
				})

			}
		})
	}



	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password !== '' && mobileNumber !== '')){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, password, mobileNumber])

	return(
		(user.id !== null)
		?
		<Navigate to="/courses" />
		: 
		<Form onSubmit={(event) => registerUser(event)} >
		<h3>Register</h3>
			<Form.Group controlId="userFirstName">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control 
			        type="firstName" 
			        placeholder="Enter First Name" 
			        value = {firstName}
			        onChange = {event => setFirstName(event.target.value)}
			        required
			    />
			</Form.Group>

			<Form.Group controlId="userLastName">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control 
			        type="lastName" 
			        placeholder="Enter Last Name" 
			        value = {lastName}
			        onChange = {event => setLastName(event.target.value)}
			        required
			    />
			</Form.Group>

	        <Form.Group controlId="userEmail">
	            <Form.Label>Email address</Form.Label>
	            <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value = {email}
	                onChange = {event => setEmail(event.target.value)}
	                required
	            />
	            <Form.Text className="text-muted">
	                We'll never share your email with anyone else.
	            </Form.Text>
	        </Form.Group>

	        <Form.Group controlId="password">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password} 
	          		onChange={event => setPassword(event.target.value)}
	                required
	            />
	        </Form.Group>

	        <Form.Group controlId="mobileNumber">
	            <Form.Label>Mobile Number</Form.Label>
	            <Form.Control 
	                type="mobileNumber" 
	                placeholder="Enter mobile number" 
	                value={mobileNumber} 
	                onChange={event => setMobileNumber(event.target.value)}
	                required
	            />
	        </Form.Group>
	        { isActive ? //true
	        	<Button variant="primary" type="submit" id="submitBtn">
	        		Submit
	        	</Button>
	        	: // false
	        	<Button variant="primary" type="submit" id="submitBtn" disabled>
	        		Submit
	        	</Button>
	        
	    	}
	        
	    </Form>
	)
}

