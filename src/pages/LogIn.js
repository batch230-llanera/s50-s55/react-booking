import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2'


export default function Login(){
	// 3 - use the state
	// variable, setter function
	const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);


	console.log(email);
	console.log(password);

	/*
	function loginUser(event){
		//Prevents page redirection via form submission
		event.preventDefault();

		// Clear input fields after submission
		localStorage.setItem('email', email);

		setUser({
			email: localStorage.getItem('email')

		})
		setEmail('');
		setPassword('');

		console.log(`${email} has been verified. Welcome back!`)

	}
	*/

	function authenticate(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: { 'Content-Type' : 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			console.log("Check accessToken");
			console.log(data.createAccessToken);

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.createAccessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt"
				})
			}
			else{
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again"
				})
			}


		})

		setEmail('');
	}

	const retrieveUserDetails = (token) => {
	        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
	            headers: {
	                Authorization: `Bearer ${token}`
	            }
	        })
	        .then(res => res.json())
	        .then(data => {
	            console.log(data);

	            setUser({
	                id: data._id,
	                isAdmin: data.isAdmin
	            })
	        })
	    }


	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both
		if(email!=='' && password!==''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])

	return(
		(user.id !== null)
		? // true - means email field is successfully set
		<Navigate to="/login" />
		: // false means email field is not successfully set
		<Form onSubmit={(event) => authenticate(event)}>
		<h3>Login</h3>
			<Form.Group controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter email"
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required
					/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password" 
					placeholder="Password"
					value = {password}
					onChange = {e => setPassword(e.target.value)}
					required
				    />
			</Form.Group><br/>

			{isActive?
				<Button variant="success" type="submit" id="submitBtn">
					Login
				</Button>
				:
				<Button variant="success" type="submit" id="submitBtn" disabled>
					Login
				</Button>
			}
			
		</Form>		
	)
}